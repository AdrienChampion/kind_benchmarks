/// Runs some benchmarks.

use std::path::PathBuf ;

use time::SteadyTime as Time ;

use super::{ Conf, Log } ;

use tools ;
use tools::{ Tool, Run, Res } ;


/// Runs all the tools specified by conf on all the lustre files in `file`.
pub fn run(log: & Log, conf: Conf, bench_file: String) -> i32 {
  use std::io::{ BufRead, BufReader } ;
  use std::fs::OpenOptions ;

  let mut path_to_benchs = PathBuf::from(& bench_file) ;
  path_to_benchs.pop() ;
  let (benchs, bench_count) = match OpenOptions::new().read(true).open(
    & bench_file
  ) {
    Ok(benchs) => (
      BufReader::new(benchs),
      BufReader::new(
        OpenOptions::new().read(true).open(& bench_file).unwrap()
      ).lines().count()
    ),
    Err(e) => {
      error!(
        log,
        ("Error opening benchmark file \"{}\":", bench_file),
        ("  {}", e)
      ) ;
      return 2
    },
  } ;
  let width = format!("{}", bench_count).len() ;

  let mut tools = match tools::of_conf(& conf) {
    Ok(tools) => tools,
    Err(s) => {
      log!(
        log, "{}: {}", log.bad("Error"), s
      ) ;
      return 2
    }
  } ;

  let mut line = 0 ;

  for file in benchs.lines() {
    line += 1 ;
    let file = match file {
      Ok(file) => file,
      Err(e) => {
        error!(
          log,
          ("Could not read line {} of file \"{}\":", line, bench_file),
          ("  {}", e)
        ) ;
        return 2
      },
    } ;
    let mut path = path_to_benchs.clone() ;
    path.push( & PathBuf::from(& file) ) ;

    begin!(
      log, "[{1:>0$}/{2}] Running on \"{3}\"",
      width, line, bench_count,
      log.emph(format!("{}", path.to_str().unwrap()))
    ) ;

    if conf.sep() {
      separate(log, & path, & mut tools, conf.out())
    } else {
      launch(log, & path, & mut tools)
    } ;

    end!(log)
  } ;

  0
}


/// Copies a file.
fn copy(log: & Log, src: & PathBuf, tgt: & str) {
  match ::std::fs::create_dir_all(tgt) {
    Ok(()) => (),
    Err(e) => {
      error!(
        log,
        ("Could not create directory \"{}\":", tgt),
        ("  {}", e)
      ) ;
      return ()
    }
  } ;
  match ::std::fs::copy(
    & src,
    & format!(
      "{}/{}", tgt, src.file_name().unwrap().to_str().unwrap()
    )
  ) {
    Ok(_) => (),
    Err(e) => error!(
      log,
      ("Could not copy file \"{}\":", src.to_str().unwrap()),
      (
        "  tried to copy it to \"{}/{}\", got:\n  {}",
        tgt, src.file_name().unwrap().to_str().unwrap(), e
      )
    ),
  }
}

/// Returns the index of the first Kind 2 `Tool` from a list of `Tool`s.
fn get_kind2_index(tools: & [ Run ]) -> Option<usize> {
  let mut cnt = 0 ;
  for tool in tools.iter() {
    if tool.tool() == & Tool::Kind2 {
      return Some(cnt)
    }
    cnt += 1 ;
  } ;
  None
}

/// Separates safe, unsafe, unknown and error systems.
fn separate(log: & Log, path: & PathBuf, tools: & mut [ Run ], tgt: & str) {
  let index = match get_kind2_index(tools) {
    Some(index) => index,
    None => {
      error!(
        log,
        ("Error in separation mode:"),
        ("  Kind 2 is deactivated")
      ) ;
      return ()
    },
  } ;
  let correct = format!("{}/safe", tgt) ;
  let failure = format!("{}/unsafe", tgt) ;
  let unknown = format!("{}/unknown", tgt) ;
  let error = format!("{}/error", tgt) ;
  launch_do(
    log, path, & mut tools[index..(index + 1)],
    |status, path| match status {
      Res::Timeout => copy(log, path, & unknown),
      Res::Unsafe => copy(log, path, & failure),
      Res::Safe => copy(log, path, & correct),
      Res::Error => copy(log, path, & error),
    }
  )
}

/// Launches the tools in sequence.
fn launch(log: & Log, path: & PathBuf, tools: & mut [ Run ]) {
  launch_do(log, path, tools, |_, _| ())
}

/// Launches the tools in sequence.
fn launch_do<F: Fn(Res, & PathBuf)>(
  log: & Log, path: & PathBuf, tools: & mut [ Run ], f: F
) {
  use std::io::Write ;
  for tool in tools.iter_mut() {
    log!(
      log, "Running {}.", tool.name()
    ) ;
    let start = Time::now() ;
    let timeout = (tool.to() * 1000) as i64 ;
    match tool.run(& path) {
      Ok(status) => {
        let time = (
          Time::now() - start
        ).num_milliseconds() ;
        f(status, path) ;
        match status {
          Res::Error => (),
          _ => match write!(
            tool, "{}{} {}\n",
            path.to_str().unwrap(),
            if status == Res::Timeout || time > timeout {
              "# "
            } else {
              ""
            },
            time
          ) {
            Ok(()) => (),
            Err(e) => error!(
              log,
              ("Could not write to stat file for {}:", tool.name()),
              ("  {}", e)
            ),
          },
        } ;
        log!(
          log, "{} in {}ms ({})",
          log.happy("> done"), log.emph(format!("{}", time)),
          status.to_str(log)
        )
      },
      Err(s) => log!(
        log, "{}: {}", log.bad("> error"), s
      ),
    }
  }
}


// fn launch_parallel(
//   log: & Log, path: & PathBuf, tools: & [Run], to: usize
// ) {
//   use tools::ParaRun ;
//   use std::sync::mpsc::TryRecvError::* ;
//   let to = to as i64 ;
//   let rcv = {
//     let (snd, rcv) = channel() ;
//     for tool in tools.iter() {
//       log!(
//         log, "Launching {}.", tool.name()
//       ) ;
//       let tool = ParaRun::mk(tool.clone(), path, snd.clone()) ;
//       spawn(
//         move || tool.run()
//       ) ;
//       ()
//     } ;
//     rcv
//   } ;
//   let start = Time::now() ;
//   loop {
//     let now = (Time::now() - start).num_seconds() ;
//     if now >= to {
//       log!(log, "Timeout, stopping remaining jobs.") ;
//       break
//     } ;
//     match rcv.try_recv() {
//       Ok( Ok( (status, bla) ) ) => {
//         log!(
//           log,
//           "{} ({}).", bla,
//           match status {
//             0 => log.emph("0"),
//             10 => log.sad("10"),
//             20 => log.happy("20"),
//             _ => log.bad( format!("{}", status) ),
//           }
//         )
//       },
//       Ok( Err(_) ) => panic!("aaa"),
//       Err(Empty) => (),
//       Err(Disconnected) => break,
//     } ;
//     sleep_ms(10)
//   }
// }