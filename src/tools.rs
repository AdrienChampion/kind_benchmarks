/// Tool info and basic functions to run them.

use std::path::PathBuf ;
use std::fs::OpenOptions as Open ;
use std::fs::File ;
use std::process::Command ;
// use std::sync::mpsc::Sender ;
use std::io ;

use self::Tool::* ;
use Log ;

/// All tools.
pub fn all() -> Vec<Tool> {
  vec![ Kind2, PKind, JKind, Zustr, NuXmv ]
}

/// Returns the tool corresponding to a flag.
pub fn of_flag(flag: & str) -> Option<Tool> {
  if flag == Kind2.flag() { return Some(Kind2) } ;
  if flag == PKind.flag() { return Some(PKind) } ;
  if flag == JKind.flag() { return Some(JKind) } ;
  if flag == Zustr.flag() { return Some(Zustr) } ;
  if flag == NuXmv.flag() { return Some(NuXmv) } ;
  None
}

/// Result of a run.
#[derive(Clone, Copy, PartialEq, Eq)]
pub enum Res {
  /// Timeout.
  Timeout,
  /// System was proved safe.
  Safe,
  /// System was proved unsafe.
  Unsafe,
  /// An error happened.
  Error,
}
impl Res {
  /// String representation.
  pub fn to_str(& self, log: & Log) -> String {
    match * self {
      Res::Timeout => format!("{}", log.emph("timeout")),
      Res::Safe => format!("{}", log.happy("safe")),
      Res::Unsafe => format!("{}", log.sad("unsafe")),
      Res::Error => format!("{}", log.bad("error")),
    }
  }
}

/// Enumeration of the different tools.
#[derive(PartialEq, Eq, Debug)]
pub enum Tool {
  /// Kind 2.
  Kind2,
  /// PKind.
  PKind,
  /// jKind.
  JKind,
  /// Zustre.
  Zustr,
  /// nuXmv
  NuXmv,
}
impl Tool {
  /// Identifier of the tool.
  pub fn id(& self) -> & 'static str {
    match * self {
      Kind2 => "kind2",
      PKind => "pkind",
      JKind => "jkind",
      Zustr => "zustre",
      NuXmv => "NuXmv",
    }
  }

  /// Name of the tool.
  pub fn name(& self) -> & 'static str {
    match * self {
      Kind2 => "Kind 2",
      PKind => "PKind",
      JKind => "jKind",
      Zustr => "Zustre",
      NuXmv => "NuXmv",
    }
  }

  /// Option flag of the tool.
  pub fn flag(& self) -> & 'static str {
    match * self {
      Kind2 => "-k2",
      PKind => "-pk",
      JKind => "-jk",
      Zustr => "-zu",
      NuXmv => "-nx"
    }
  }

  /// Prefix and arguments to run the tool with.
  pub fn args(& self, to: usize) -> Vec<String> {
    let to = format!("{}", to) ;
    match * self {
      Kind2 => vec![
        "--timeout_wall".to_string(), to
      ],
      PKind => vec![
        "-compression".to_string(),
        "-with-inv-gen".to_string(),
        "-n".to_string(), "100000000".to_string(),
        "-timeout".to_string(), format!("{}.0",to)
      ],
      JKind => vec![
        "-solver".to_string(), "z3".to_string(),
        "-timeout".to_string(), to
      ],
      Zustr => vec![
        "--timeout".to_string(), format!("{}.0", to)
      ],
      NuXmv => vec![ format!("{}", to) ],
    }
  }

  /// File suffix.
  pub fn suff(& self) -> & 'static str {
    match * self {
      PKind | Zustr => ".bang.lus",
      NuXmv => ".smv",
      _ => "",
    }
  }

  /** String to look for when analyzing a run. Returns
  - success tags (safe),
  - failure tags (unsafe),
  - error tags. */
  fn tags(& self) -> (
    Vec<& 'static str>,
    Vec<& 'static str>,
    Vec<& 'static str>,
  ) {
    let (succ, fail, err) = match * self {
      Kind2 => (
        vec!["Success"],
        vec!["Failure"],
        vec!["Error", "error"]
      ),
      PKind => (
        vec!["valid", "SAFE"],
        vec!["Counterexample", "CEX"],
        vec!["Error", "error"],
      ),
      JKind => (
        vec!["VALID"],
        vec!["INVALID"],
        vec!["Exception", "Error", "error"],
      ),
      Zustr => (
        vec!["SAFE"],
        vec!["CEX"],
        vec!["Error", "error"],
      ),
      NuXmv => (
        vec!["is true"],
        vec!["is false"],
        vec!["error", "cannot"],
      ),
    } ;

    (
      succ.into_iter().fold(
        vec![], |mut v, s| { v.push("-e") ; v.push(s) ; v }
      ),
      fail.into_iter().fold(
        vec![], |mut v, s| { v.push("-e") ; v.push(s) ; v }
      ),
      err.into_iter().fold(
        vec![], |mut v, s| { v.push("-e") ; v.push(s) ; v }
      )
    )
  }

  /// Analyzes a run.
  pub fn outcome(& self, out: & str, out_err: & str) -> Res {
    use std::str::{ from_utf8, FromStr } ;
    let (succ, fail, err) = self.tags() ;

    let succ = match Command::new(
      "grep"
    ).arg("-c").arg(out).args(& succ).output() {
      Ok(output) => {
        let stdout = from_utf8(
          & output.stdout
        ).unwrap().lines().next().unwrap().to_string() ;
        let count = usize::from_str(& stdout).unwrap() ;
        count > 0
      },
      Err(e) => panic!("could not open stdout file \"{}\":\n{}", out, e)
    } ;

    let fail = match Command::new(
      "grep"
    ).arg("-c").arg(out).args(& fail).output() {
      Ok(output) => {
        let stdout = from_utf8(
          & output.stdout
        ).unwrap().lines().next().unwrap().to_string() ;
        let count = usize::from_str(& stdout).unwrap() ;
        count > 0
      },
      Err(e) => panic!("could not open stdout file \"{}\"\n{}", out, e)
    } ;

    let err = match Command::new(
      "grep"
    ).arg("-c").arg(out).args(& err).output() {
      Ok(output) => {
        let stdout = from_utf8(
          & output.stdout
        ).unwrap().lines().next().unwrap().to_string() ;
        let count = usize::from_str(& stdout).unwrap() ;
        count > 0
      },
      Err(e) => panic!("could not open stdout file \"{}\"\n{}", out, e)
    } || match Command::new(
      "grep"
    ).arg("-c").arg(out_err).args(& err).output() {
      Ok(output) => {
        let stdout = from_utf8(
          & output.stdout
        ).unwrap().lines().next().unwrap().to_string() ;
        let count = usize::from_str(& stdout).unwrap() ;
        count > 0
      },
      Err(e) => panic!("could not open stdout file \"{}\"\n{}", out_err, e)
    } ;

    if err { Res::Error } else {
      if fail { Res::Unsafe } else {
        if succ { Res::Safe } else { Res::Timeout }
      }
    }
  }
}

macro_rules! from_conf {
  (
    $vec:ident <- $tool:ident ($field:ident) from $conf:expr) => (
    match $conf.$field {
      Some(ref cmd) => {
        let tool = Tool::$tool ;
        let log = format!("{}/{}", $conf.res_dir, tool.id()) ;
        match ::std::fs::create_dir_all(& log) {
          Ok(()) => (),
          Err(e) => return Err(
            format!(
              "could not create log directory \"{}\":\n  {}", log, e
            )
          ),
        } ;
        let file = format!("{}/stats", log) ;
        let file = match Open::new(
        ).create(true).write(true).truncate(true).open(& file) {
          Ok(file) => file,
          Err(e) => return Err(
            format!("Could not access stat file \"{}\":\n  {}", file, e)
          ),
        } ;
        let args = tool.args($conf.to()) ;
        $vec.push(
          Run::mk(
            tool,
            cmd.to_string(),
            args,
            log,
            $conf.to,
            file,
          )
        )
      },
      None => ()
    }
  ) ;
}

/// Creates the `Run`s corresponding to a `Conf`.
pub fn of_conf(conf: & super::Conf) -> Result<Vec<Run>, String> {
  let mut vec = vec![] ;
  from_conf!(vec <- Kind2(kind2) from conf) ;
  from_conf!(vec <- PKind(pkind) from conf) ;
  from_conf!(vec <- JKind(jkind) from conf) ;
  from_conf!(vec <- Zustr(zustr) from conf) ;
  from_conf!(vec <- NuXmv(nuxmv) from conf) ;
  Ok(vec)
}

/// Can run a tool.
pub struct Run {
  /// Kind of tool to run.
  tool: Tool,
  /// Command to run it with.
  cmd: String,
  /// Arguments to run it with.
  args: Vec<String>,
  /// Log prefix.
  log: String,
  /// Timeout in milliseconds.
  to: usize,
  /// Result file.
  file: File,
}

impl Run {
  /// Creates a new `Run`.
  pub fn mk(
    tool: Tool, cmd: String, args: Vec<String>,
    log: String, to: usize, mut file: File
  ) -> Self {
    use std::io::Write ;
    write!(& mut file, "# {}\n", tool.name()).unwrap() ;
    Run { tool: tool, cmd: cmd, args: args, log: log, to: to, file: file }
  }

  /// Tool of the `Run`.
  #[inline(always)]
  pub fn tool(& self) -> & Tool { & self.tool }
  /// Name of the tool.
  #[inline(always)]
  pub fn name(& self) -> & 'static str { self.tool.name() }
  /// Timeout.
  #[inline(always)]
  pub fn to(& self) -> usize { self.to }

  /// Runs the tool on a file.
  pub fn run(& self, file_path: & PathBuf) -> Result<Res, String> {
    use std::io::Write ;

    let suff = self.tool.suff() ;
    let mut file_path = file_path.clone() ;
    let file_name = format!("{}{}", file_path.file_name().unwrap().to_str().unwrap(), suff) ;
    file_path.set_file_name(file_name) ;

    let (log,log_err) = log_of(& self.log, & file_path) ;
    let mut log_file = match Open::new(
    ).write(true).create(true).truncate(true).open(& log) {
      Ok(file) => file,
      Err(e) => return Err(
        format!("could not open log file \"{}\":\n  {}", log, e)
      ),
    } ;
    let mut log_err_file = match Open::new(
    ).write(true).create(true).truncate(true).open(& log_err) {
      Ok(file) => file,
      Err(e) => return Err(
        format!("could not open log file \"{}\":\n  {}", log_err, e)
      ),
    } ;
    match Command::new(
      & self.cmd
    ).args(& self.args).arg(& file_path).output() {
      Ok(output) => {
        log_file.write(& output.stdout).unwrap() ;
        log_err_file.write(& output.stderr).unwrap() ;
        Ok( self.tool.outcome(& log, & log_err) )
      },
      Err(e) => Err(
        format!(
          "could not run {} on file \"{}\": \n  {}",
          self.tool.name(), file_path.to_str().unwrap(), e
        )
      ),
    }
  }
}

impl io::Write for Run {
  fn write(& mut self, buf: & [u8]) -> io::Result<usize> {
    self.file.write(buf)
  }
  fn flush(& mut self) -> io::Result<()> {
    self.file.flush()
  }
}

/** Returns the normal and error log files corresponding to a log prefix (dir)
and a file. */
fn log_of(pref: & str, file: & PathBuf) -> (String, String) {
  let _file = file.clone() ;
  let name = _file.as_path().file_name().unwrap().to_str().unwrap() ;
  (
    format!(
      "{}/{}.out", pref,
      name
    ),
    format!(
      "{}/{}.err", pref,
      name
    )
  )
}


// /// Can run a benchmark in a sub-thread.
// pub struct ParaRun {
//   /// The tool to run.
//   run: Run,
//   /// The benchmark to run.
//   bench: String,
//   /// Communication channel with the supervisor (up).
//   channel_up: Sender< Result<(i32, String), String> >,
// }
// impl ParaRun {
//   /// Creates a new `ParaRun`.
//   pub fn mk(
//     run: Run,
//     bench: & PathBuf,
//     channel_up: Sender< Result<(i32, String), String> >,
//   ) -> Self {
//     ParaRun {
//       run: run,
//       bench: format!("{}", bench.to_str().unwrap()),
//       channel_up: channel_up
//     }
//   }

//   /// Launches the run.
//   pub fn run(& self) {
//     use time::SteadyTime as Time ;
//     let start = Time::now() ;
//     match self.channel_up.send(
//       match self.run.run( & PathBuf::from(& self.bench) ) {
//         Ok(status) => Ok(
//           (
//             status,
//             format!(
//               "{} done in {}ms",
//               self.run.name(),
//               (Time::now() - start).num_milliseconds()
//             )
//           )
//         ),
//         Err(s) => Err(s),
//       }
//     ) {
//       Ok(()) => (),
//       Err(_) => (),
//     }
//   }
// }