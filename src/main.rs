#![allow(non_upper_case_globals)]

extern crate ansi_term as ansi ;
extern crate time ;

use std::borrow::Cow ;
use std::env::args ;
use std::process::exit ;

use ansi::{ Style, Colour } ;
use ansi::ANSIString as Str ;

/// Given a log, formats its argument and prints the lines.
#[macro_export]
macro_rules! log {
  ($log:expr, $s:expr, $($arg:expr),*) => (
    for line in format!( $s $( , $arg )* ).lines() {
      println!("{} {}", $log.pref, line)
    }
  ) ;
  ($log:expr, $s:expr) => ( log!($log, $s,) ) ;
}

/// Given a log and a painter, prints a title and some lines.
#[macro_export]
macro_rules! fmt_log {
  (
    $log:expr,
    $paint:expr,
    ( $title:expr, $( $ttl_arg:expr ),* ),
    ( $text:expr, $( $txt_arg:expr ),* )
  ) => (
    {
      println!(
        "{} {}", $paint.paint($log.head), format!($title, $($ttl_arg),*)
      ) ;
      for line in format!( $text, $($txt_arg),* ).lines() {
        println!("{} {}", $paint.paint($log.pref), line)
      } ;
      println!("{}", $paint.paint($log.trail)) ;
      $log.nl()
    }
  ) ;
  (
    $log:expr,
    $paint:expr,
    ( $title:expr ),
    ( $text:expr )
  ) => (
    fmt_log!( $log, $paint, ($title,), ($text,) )
  ) ;
  (
    $log:expr,
    $paint:expr,
    ( $title:expr ),
    $text:tt
  ) => (
    fmt_log!( $log, $paint, ($title,), $text )
  ) ;
  (
    $log:expr,
    $paint:expr,
    $title:tt,
    ( $text:expr )
  ) => (
    fmt_log!( $log, $paint, $title, ($text,) )
  ) ;
}

/// Given a log, prints a success title and some lines.
#[macro_export]
macro_rules! success {
  (
    $log:expr,
    $ttl:tt,
    $txt:tt
  ) => (
    fmt_log!( $log, $log.happy, $ttl, $txt )
  ) ;
}

/// Given a log, prints a warning title and some lines.
#[macro_export]
macro_rules! warning {
  (
    $log:expr,
    $ttl:tt,
    $txt:tt
  ) => (
    fmt_log!( $log, $log.sad, $ttl, $txt )
  ) ;
}

/// Given a log, prints an error title and some lines.
#[macro_export]
macro_rules! error {
  (
    $log:expr,
    $ttl:tt,
    $txt:tt
  ) => (
    fmt_log!( $log, $log.bad, $ttl, $txt )
  ) ;
}

/// Given a log, prints a title and some lines.
#[macro_export]
macro_rules! normal {
  (
    $log:expr,
    $ttl:tt,
    $txt:tt
  ) => (
    fmt_log!( $log, Style::new(), $ttl, $txt )
  ) ;
}

/// Prints a title with a title symbol.
#[macro_export]
macro_rules! title {
  ($log:expr, $ttl:expr, $($arg:expr),*) => (
    println!(
      "{} {} {}",
      $log.pref,
      $log.emph($log.title),
      $log.emph( format!($ttl, $($arg),*) )
    ) ;
  ) ;
  ($log:expr, $ttl:expr) => ( title!($log, $ttl,) ) ;
}

/// Prints a header.
#[macro_export]
macro_rules! begin {
  ($log:expr, $ttl:expr, $($arg:expr),*) => (
    println!(
      "{} {}", $log.head, format!($ttl, $($arg),*)
    ) ;
  ) ;
  ($log:expr, $ttl:expr) => ( begin!($log, $ttl,) ) ;
}

/// Prints a trailer.
#[macro_export]
macro_rules! end {
  ($log:expr) => ( println!("{}\n", $log.trail) ) ;
}

pub mod tools ;
use tools::Tool::* ;
pub mod run ;
pub mod draw ;

/// Output directory flag.
const out_flg: & 'static str = "-o"   ;
/// Timeout flag.
const to_flg:  & 'static str = "-to"  ;
/// Separation flag.
const sep_flg: & 'static str = "-sep" ;
/// Help flag.
const hlp_flg: & 'static str = "-h"   ;

pub struct Log {
  emph: Style,
  happy: Style,
  sad: Style,
  bad: Style,
  pub head: & 'static str,
  pub title: & 'static str,
  pub pref: & 'static str,
  pub trail: & 'static str,
}
impl Log {
  pub fn mk() -> Self {
    Log {
      emph: Style::new().bold(),
      happy: Colour::Green.normal(),
      sad: Colour::Yellow.normal(),
      bad: Colour::Red.normal(),
      head: "|=====|",
      title: "#",
      pref: "|",
      trail: "|===|",
    }
  }
  pub fn emph<'a, S: Into<Cow<'a,str>>>(& self, s: S) -> Str<'a> {
    self.emph.paint(s)
  }
  pub fn happy<'a, S: Into<Cow<'a,str>>>(& self, s: S) -> Str<'a> {
    self.happy.paint(s)
  }
  pub fn sad<'a, S: Into<Cow<'a,str>>>(& self, s: S) -> Str<'a> {
    self.sad.paint(s)
  }
  pub fn bad<'a, S: Into<Cow<'a,str>>>(& self, s: S) -> Str<'a> {
    self.bad.paint(s)
  }

  pub fn nl(& self) { println!("") }
}

fn help(log: & Log) {
  let default = Conf::default() ;
  fmt_log!(
    log,
    log.emph,
    (
      "Usage: {}",
      log.emph(
        format!(
          "{} [ {} | draw [<file>]+ | run [option]* <file> ]",
          args().next().unwrap(),
          hlp_flg
        )
      )
    ),
    (
      "\
{}
  Prints this message.
{} [<file>]+:
  Draws the graph for one or more runs from their stat files.
{} [option]* <file>:
  Runs some tools on all the files listed in <file>, where [option] is one of
  [{}] [\"<cmd>\" | no]
    sets a custom command for running a tool, or deactivates it from the
    benchmark run with `no`. Flags correspond to{}
    {}: nuxmv must be wrapped for this software to use it, more details at
    https://bitbucket.org/AdrienChampion/kind_benchmarks
  {} <dir>
    sets the output directory.
    Default: \"{}\".
  {} <int>
    sets a timeout for each run, in seconds.
    Default: {}.
  {}
    run in separation mode. Will only run Kind 2 and use the exit status
    convention to copy safe, unsafe, error and unknown files to corresponding
    directories in the output directory.
    Exit status convention is
      0  | unknown (timeout)
      10 | unsafe  (at least one property falsifiable)
      20 | safe    (all properties are invariant)
      _  | error\
      ",
      log.emph(hlp_flg),
      log.emph("draw"),
      log.emph("run"),
      tools::all().into_iter().fold(
        String::new(),
        |s, tool| format!(
          "{}{}{}",
          s,
          if s.is_empty() { "" } else { "|" },
          log.emph(tool.flag())
        )
      ),
      tools::all().into_iter().fold(
        String::new(),
        |s, tool| format!(
          "{}\n    {}{}: {}\n      {}",
          s,
          if s.is_empty() { "" } else { "" },
          tool.flag(),
          log.emph(tool.name()),
          tool.args(default.to()).iter().fold(
            default.command(& tool).unwrap().to_string(),
            |s, arg| format!(
              "{}{}{}",
              s,
              " ",
              arg
            )
          )
        )
      ),
      log.emph("NB"),
      log.emph(out_flg),
      default.res_dir,
      log.emph(to_flg),
      default.to,
      log.emph(sep_flg)
    )
  )
}

/// Modes of the binary.
pub enum Mode {
  /// Run mode. Stores the configuration and the bench file.
  Run(Conf, String),
  /// Draw mode. Stores the two stat files to compare.
  Draw(Vec<String>),
}

/// Stores the configuration of the benchmark run.
pub struct Conf {
  /// Kind 2 command.
  kind2: Option<String>,
  /// PKind command.
  pkind: Option<String>,
  /// JKind command.
  jkind: Option<String>,
  /// Zustre command.
  zustr: Option<String>,
  /// NuXmv command.
  nuxmv: Option<String>,
  /// Result directory.
  res_dir: String,
  /// Timeout.
  to: usize,
  /// Separation mode.
  sep: bool,
}
impl Conf {
  /// Default configuration.
  fn default() -> Self {
    let path = "./res".to_string() ;
    Conf {
      kind2: Some("kind2".to_string()),
      pkind: Some("pkind".to_string()),
      jkind: Some("jkind".to_string()),
      zustr: Some("zustre".to_string()),
      nuxmv: Some("nuxmv".to_string()),
      res_dir: path,
      to: 300,
      sep: false,
    }
  }

  /// Command of a tool.
  fn command(& self, tool: & tools::Tool) -> Option<& String> {
    match * tool {
      Kind2 => self.kind2.as_ref(),
      PKind => self.pkind.as_ref(),
      JKind => self.jkind.as_ref(),
      Zustr => self.zustr.as_ref(),
      NuXmv => self.nuxmv.as_ref(),
    }
  }

  /// Parses the CLAs and constructs the configuration.
  pub fn mk() -> Result<Option< Mode >, String> {
    use std::str::FromStr ;

    let mut conf = Conf::default() ;
    let mut args = args() ;
    // Discarding first argument as it's the `bench` command.
    args.next().unwrap() ;
    match args.next() {
      Some(ref head) if head == "run" => (),
      Some(ref head) if head == "draw" => {
        let mut vec = vec![] ;
        match (args.next(), args.next()) {
          (Some(f1), Some(f2)) => {
            vec.push(f1.to_string()) ;
            vec.push(f2.to_string())
          },
          (Some(f), None) => {
            vec.push(f.to_string())
          },
          _ => return Err(
            "expected path to stat file in draw mode, \
            found nothing".to_string()
          ),
        } ;
        for f in args.into_iter() {
          vec.push(f)
        } ;
        return Ok( Some( Mode::Draw(vec) ) )
      },
      // Help.
      Some(ref head) if head == hlp_flg => return Ok( None ),
      Some(ref head) => return Err(
        format!(
          "unexpected first argument \"{}\"", head
        )
      ),
      None => return Err(
        "expected at least one argument, found nothing".to_string()
      ),
    } ;
    loop {
      if let Some(nxt) = args.next() {
        match tools::of_flag(& nxt) {
          Some(tool) => {
            let cmd = match args.next() {
              Some(ref arg) if arg == "no" => None,
              Some(arg) => Some(arg.to_string()),
              None => return Err(
                format!(
                  "expected command for Kind 2 after \"{}\", found nothing",
                  nxt
                )
              ),
            } ;
            match tool {
              Kind2 => conf.kind2 = cmd,
              PKind => conf.pkind = cmd,
              JKind => conf.jkind = cmd,
              Zustr => conf.zustr = cmd,
              NuXmv => conf.nuxmv = cmd,
            }
          },

          // Output directory.
          _ if nxt == out_flg => match args.next() {
            Some(dir) => conf.res_dir = dir.to_string(),
            None => return Err(
              format!(
                "expected path after \"{}\", found nothing",
                out_flg
              )
            ),
          },

          // Timeout.
          _ if nxt == to_flg => match args.next() {
            Some(to) => match usize::from_str(& to) {
              Ok(to) => conf.to = to,
              Err(_) => return Err(
                format!(
                  "expected integer after \"{}\", found \"{}\"",
                  to_flg, to
                )
              )
            },
            None => return Err(
              format!(
                "expected integer after \"{}\", found nothing",
                to_flg
              )
            ),
          },

          // Separation.
          _ if nxt == sep_flg => conf.sep = true,

          // Help.
          _ if nxt == hlp_flg => return Ok( None ),

          // Last argument.
          _ => match args.next() {
            None => return Ok(
              Some( Mode::Run(conf, nxt.to_string()) )
            ),
            Some(arg) => return Err(
              format!(
                "unexpected argument \"{}\"\nafter bench path \"{}\"",
                arg, nxt
              )
            ),
          },
        }
      } else {
        return Err(
          "expected path to benchmarks but found nothing".to_string()
        )
      }
    }
  }

  /// True if we're running in separation mode.
  pub fn sep(& self) -> bool { self.sep }

  /// Output directory.
  pub fn out(& self) -> & str { & self.res_dir }

  /// Timeout.
  pub fn to(& self) -> usize { self.to }

  /// Prints a short description of the configuration.
  pub fn print(& self, log: & Log, dir: & str) {
    normal!(
      log,
      ("Running on all benchmarks in \"{}\"", dir),
      (
        "tools:{}{}{}{}{}\noutput dir: \"{}\"",
        if let Some(ref cmd) = self.kind2 {
          format!("\n  {}  {} {}", "kind2", log.emph(">"), cmd)
        } else { "".to_string() },
        if let Some(ref cmd) = self.pkind {
          format!("\n  {}  {} {}", "pkind", log.emph(">"), cmd)
        } else { "".to_string() },
        if let Some(ref cmd) = self.jkind {
          format!("\n  {}  {} {}", "jkind", log.emph(">"), cmd)
        } else { "".to_string() },
        if let Some(ref cmd) = self.zustr {
          format!("\n  {} {} {}", "zustre", log.emph(">"), cmd)
        } else { "".to_string() },
        if let Some(ref cmd) = self.nuxmv {
          format!("\n  {} {} {}", "nuxmv", log.emph(">"), cmd)
        } else { "".to_string() },
        self.res_dir
      )
    )
  }
}

/// Entry point.
fn main() {
  use Mode::* ;
  let log = Log::mk() ;
  log.nl() ;
  log.nl() ;

  let status = match Conf::mk() {
    Ok( Some( Run(conf, dir) ) ) => {
      conf.print(& log, & dir) ;
      run::run(& log, conf, dir)
    },
    Ok( Some( Draw(files) ) ) => {
      draw::run(& log, files)
    },
    Ok( None ) => {
      help(& log) ;
      0
    },
    Err(s) => {
      help(& log) ;
      error!(log, ("Command line argument error:"), ("{}", s)) ;
      2
    },
  } ;

  log.nl() ;
  exit(status)
}





