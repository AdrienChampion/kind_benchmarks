//! Draws a cumulative graph of some stat files.

use std::fs::create_dir_all as mk_dir ;
use std::process::Command ;
use std::path::PathBuf ;
use std::fs::OpenOptions ;
use std::io::{ Write, BufReader, BufRead } ;

use super::Log ;

/// Gnuplot color theme.
pub struct Theme {
  /// Background.
  pub back: & 'static str,
  /// Foreground.
  pub fore: & 'static str,
  /// Line colors.
  pub line: Vec<& 'static str>,
}
impl Theme {
  /// Cool theme.
  pub fn cool() -> Self {
    Theme {
      back: "#000000",
      fore: "#33CCFF",
      line: vec![
        "#F96565", // Red.
        "#99FF99", // Green.
        "#FF8533", // Orange.
        "#BCBCBC", // Gray.
        "#F5FF5C", // Yellow.
      ]
    }
  }
  /// Crap theme.
  pub fn crap() -> Self {
    Theme {
      back: "#FFFFFF",
      fore: "#000000",
      line: vec![
        "#F96565", // Red.
        "#99FF99", // Green.
        "#FF8533", // Orange.
        "#BCBCBC", // Gray.
        "#F5FF5C", // Yellow.
      ]
    }
  }
}

/** Retrieves the names of the tools, checks bench count match. Associates an
index to each file (for theming and line coloring).

Normal result is a vector of index / name / file triples and the total number
of benchs. */
pub fn process_files(files: Vec<String>) -> Result<
  (Vec<(usize, String, String, usize)>, usize), String
> {
  let mut vec = Vec::with_capacity(files.len()) ;
  let mut bench_count = None ;
  let mut index = 0 ;
  for file_path in files.into_iter() {
    let file = match OpenOptions::new().read(true).open(& file_path) {
      Ok(file) => BufReader::new(file),
      Err(e) => return Err( format!("{}", e) ),
    } ;

    let mut lines = file.lines() ;

    let name = if let Some(Ok(line)) = lines.next() {
      let mut chars = line.chars() ;
      match (chars.next(), chars.next()) {
        (Some('#'), Some(' ')) => chars.as_str().to_string(),
        _ => return Err(
          format!("file \"{}\" is not a legal stat file", file_path)
        ),
      }
    } else {
      return Err( format!("file \"{}\" is empty", file_path) )
    } ;

    let this_bench_count = lines.count() ;
    match bench_count {
      Some(c) => if c != this_bench_count {
        bench_count = Some(::std::cmp::max(c, this_bench_count))
      },
      None => bench_count = Some(this_bench_count),
    } ;

    vec.push( (index, name, file_path, this_bench_count) ) ;

    index += 1 ;
  } ;
  match bench_count {
    Some(c) => Ok( (vec, c) ),
    None => Err(
      "could not retreive number of benchmarks".to_string()
    ),
  }
}

/// Draws the plot comparing some runs.
pub fn run(log: & Log, files: Vec<String>) -> i32 {
  use std::str::from_utf8 as str_of_bytes ;
  let plot_path = "plot" ;
  let pdf_plot_path = "plot.pdf" ;

  let theme = Theme::cool() ;
  if files.len() > theme.line.len() {
    error!(
      log,
      ("Theme error:"),
      ("  can't handle that many stats to compare (max {}).", theme.line.len())
    ) ;
    return 2
  } ;

  let (files, bench_count) = match process_files(files) {
    Ok(res) => res,
    Err(e) => {
      error!(
        log,
        ("Error processing stat files:"),
        ("{}", e)
      ) ;
      return 2
    },
  } ;

  let dir = {
    let mut buf = PathBuf::from(& files[0].2) ;
    buf.pop() ;
    buf.pop() ;
    buf.to_str().unwrap().to_string()
  } ;

  begin!(
    log,
    "Drawing comparison plot for"
  ) ;
  log!(
    log,
    "{}\non {} benchs",
    files.iter().fold(
      String::new(),
      |s, & (_, ref name, ref file, ref count)| format!(
        "{}{}{} ({}, {})",
        s, if s.is_empty() { "" } else { "\n" },
        log.emph(file.to_string()), name, count
      )
    ),
    bench_count
  ) ;
  match mk_dir(& dir) {
    Ok(()) => (),
    Err(e) => {
      error!(
        log,
        ("Error creating output directory:"),
        ("  {}", e)
      ) ;
      return 2
    },
  } ;
  
  let mut plot = PathBuf::from(& dir) ;
  plot.push(plot_path) ;
  let mut pdf_plot = PathBuf::from(& dir) ;
  pdf_plot.push(pdf_plot_path) ;


  let mut file = match OpenOptions::new(
  ).create(true).write(true).truncate(true).open(& plot) {
    Ok(file) => file,
    Err(e) => {
      error!(
        log,
        ("Error creating result file:"),
        ("  {}", e)
      ) ;
      return 2
    }
  } ;

  match write!(
    & mut file,
    // First param is background (`{0}`), second is foreground (`{1}`).
    "\
set border linecolor rgbcolor \"{1}\"
set key textcolor rgbcolor \"{1}\"
set term pdf enhanced dashed \\
  font \"Futura,12\" \\
  background rgb \"{0}\"

{5}

{6}

set xlabel \"Benchmarks solved\" textcolor rgbcolor \"{1}\"
set ylabel \"Time in ms\" textcolor rgbcolor \"{1}\"

set output \"{2}\"
set key top left
set xrange [{3}:{4}]
set yrange [100:*]

set logscale y
set format y \"10e%T\"

plot {7}\n\
    ",
    theme.back,
    theme.fore,
    pdf_plot.to_str().unwrap(),
    0.1,
    bench_count + (bench_count / 10),
    files.iter().fold(
      String::new(),
      |s, & (cnt, _, _, _)| format!(
        "{}\nset style line {} lt 1 lw 1.5 pt 3 linecolor rgb \"{}\"",
        s, cnt + 1, theme.line[cnt]
      )
    ),
    files.iter().fold(
      String::new(),
      |s, & (ref cnt, _, ref file, _)| format!(
        "\
{}set table 'temp_{}.plot'
plot '{}' using 2 smooth cumul
unset table\n\
        ",
        s, cnt, file
      )
    ),
    files.iter().fold(
      String::new(),
      |s, & (ref cnt, ref name, _, ref count)| format!(
        "{}{}\n  \"temp_{}.plot\" u 1:2 t '{} ({} solved)' w l ls {}",
        s, if s.is_empty() { "\\" } else { ", \\" },
        cnt, name, count, cnt + 1
      )
    ),
  ) {
    Ok(()) => (),
    Err(e) => {
      error!(
        log,
        ("Error writing gnuplot file:"),
        ("  {}", e)
      ) ;
      return 2
    },
  } ;

  match Command::new("gnuplot").arg(& plot).output() {
    Ok(out) => {
      if ! out.status.success() {
        error!(
          log,
          ("Error running gnuplot:"),
          ("  \
            exit status: {}\n  \
            stdout: {{\n{}\n}} \
            stderr: {{\n{}\n}}",
            log.emph(out.status.code().unwrap().to_string()),
            str_of_bytes(& out.stdout).unwrap(),
            str_of_bytes(& out.stderr).unwrap()
          )
        ) ;
        return 2
      }
    },
    Err(e) => {
      error!(
        log,
        ("Error running gnuplot:"),
        ("  {}", e)
      ) ;
      return 2
    },
  }

  success!(
    log,
    ("Success"),
    ("generated comparison plot successfully on {} benchmarks", bench_count)
  ) ;

  0
}