#!/bin/bash

nuxmv_cmd="nuxmv"

if [ "$#" -ne "2" ]
then
  echo "Error: \
    expected two arguments (timeout in sec an file), but got $# arguments\
  "
  exit 2
fi

timeout="$1"
file="$2"

echo "timeout: $timeout"
echo "file: $2"

echo -e "\
read_model -i $file
flatten_hierarchy
encode_variables
go_msat
check_invar_ic3 -i -d
quit
" | timeout $timeout $nuxmv_cmd -int