Benchmarking tool for Lustre model checkers.

# Building and running

Build with
```
cargo build --release
```
to generate a binary (`target/release/bench`).

Run
```
./target/release/bench -h
```
for help.

# Features

This tool has two modes presented below.

For more information, run this tool with `-h`.

**Run**: runs tools on benchmarks, or separates a set of benchmarks (option `-s`) depending on the outcome of running Kind 2: safe, unsafe, error and timeout.

**Draw**: produces a nice cumulative plot in gnuplot of the results of a run, using the `stats` files in the result directories of each tool.

# Tools supported

* [Kind 2][kind2]
* [JKind][jkind]
* [PKind][pkind]
* [Zustre][zustre]
* [NuXmv][nuxmv]

**NB**: NuXmv is only supported through a wrapper which, given a timeout in seconds and a file, runs NuXmv with that timeout on that file. Such a wrapper is provided in `scripts/nuxmv.sh`.

# Benchmarks

Benchmarks are located in `rsc/` and are split in three categories:

* `easy`: safe systems Kind 2 can verify in less than 100 seconds
* `unsafe`: unsafe systems Kind 2 can verify in less than 100 seconds
* `hard`: systems Kind 2 needs more than 100 seconds to verify

# Documentation

Run
```
cargo doc
```
to generate the documentation, the top-level of which is `target/doc/bench/index.html`.

[kind2]: http://kind2-mc.github.io/kind2/ (Kind 2)
[jkind]: https://github.com/agacek/jkind (JKind)
[pkind]: https://bitbucket.org/lememta/pkind (PKind)
[zustre]: https://github.com/coco-team/zustre (Zustre)
[nuxmv]: https://es-static.fbk.eu/tools/nuxmv/ (NuXmv)