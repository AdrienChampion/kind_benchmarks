#!/bin/bash

for file in `find . -iname "*.lus"` ; do
    tmp="$file.bang.lus"
    cat $file | sed -e 's/--%PROPERTY\(.*\);/--!PROPERTY:\1;/g' > $tmp
    # mv $tmp "$file.bang.lus"
done
